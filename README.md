# Arduino Tone

## Purpose

This simple sketch is designed to annoy anyone within earshot.  It randomly
beeps, with a random frequency, a random number of times at random intervals.

It is designed to use a deep sleep mode, and should only draw 6uA while it isn't
beeping.  Given a decent 5V power supply, this sketch should run without
draining your battery in any meaningful way.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/arduino_tone.git

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/arduino_tone/src/63d9f7fb2a91109ee8f195e54c8393212d59e8ef/LICENSE.txt?at=master) file for
details.

