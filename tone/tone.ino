/*
   Michael Dill - 20170301

   This sketch is designed to pick a random frequency (MHz), a random duration,
   a random number of beeps, and a random wait time.

   It will follow these random patterns, and multiply the wait time by 8
   seconds.  

   */

// Pick min and max wait times, between beeps, in minutes
byte minWait = 1;
byte maxWait = 5;

const byte spPin = 11;

// Watchdog interrupt
ISR( WDT_vect){
}

void setup(){
    // Set all pins as outputs, to save a few mA
    for( byte i = 0; i < 20; i++ ){
        pinMode( i, OUTPUT );
    }
        
    // Make sure min is smaller than max
    if( minWait > maxWait ){
        byte holding = minWait;
        minWait = maxWait;
        maxWait = holding;
    }

    // Set up watchdog timer
    WDTCSR = ( 24 );  // Change enable and WDE - also resets
    WDTCSR = ( 33 );  // Prescalers only - get rid of the WDE and WDCE bit
    WDTCSR |= ( 1 << 6 );  // Enable interrupt mode

    ADCSRA &= ~( 1 << 7 );  // Disable ADC
    SMCR |= ( 1 << 2 );  // Set power down mode
    SMCR |= 1; // Enable sleep

    // Run through some tones, to assure we're working
    tone( spPin, 2500, 800 );
    delay( 200 );
    tone( spPin, 500, 800 );
    delay( 200 );
    tone( spPin, 2500, 800 );
    delay( 200 );
}

void loop(){
    byte beeps = random( 1, 3 );  // Pick a random number of beeps
    int duration = random( 50, 200 );  // Get a random duration for our beeps
    int wait = random( ( minWait * 60 / 8 ), ( maxWait * 60 / 8 ) );  
            // Pick a multiple of 8s to wait to repeat based on user settings

    for( int i = 0; i <= beeps; i++ ){
        tone( spPin, random( 31, 4000 ), duration );
        delay( random( 2, 2000 ) / 4 );
    }

    noTone( spPin );

    for( int i = 0; i < wait; i++ ){  // Wait 8 seconds at a time
        //Disable BOD
        MCUCR |= ( 3 << 5 );  // Set both BODS and BODSE at the same time
        MCUCR = ( MCUCR & ~( 1 << 5 ) ) | ( 1 << 6 );
                // Then set the BODS bit to clear the BODSE at the same time

        __asm__  __volatile__( "sleep" );
    }
}

